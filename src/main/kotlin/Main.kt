import kotlin.math.floor
import kotlin.math.pow

class bilanganBulat(val pembulat: Double){          //Kelas untuk menentukan bilangan bulat
    var bilBulat : Int = 0
    set(value) {
        field = if (pembulat < 0.5) value
        else value + 1
    }
}

fun main(args: Array<String>) {
    val p : Int                                    //variabel untuk menentukan pembulatan dibelakang(1..) atau didpan koma ((-1)...)
    val angka : Double                             //Variabel untuk menyimpan desimal atau inputan user

    println("\t*Program Pembulatan Bilangan Positif*\n")    //Proses menginput angka/desimal
    do{
        print("Masukkan Angka Desimal = ")
        val input1 = readLine()                     //menyimpan sementara inputan user berupa string
        print("Tentukan Pembulatan Angka (1~...(dibelakang koma) atau ....~(-1)(Satuan terdekat)) = ")  //input > 0  untuk pembulatan dibelakang koma
        val input2 = readLine()                                                                         //input < 0 (negatif) untuk pembulatan didepan koma
        val cek1 = input1!!.toDoubleOrNull()        //Mengecek inputan user sebelumnya sudah sesuai atau tidak dengan tipe data yang diinginkan sistem
        val cek2 = input2!!.toIntOrNull()
        if(cek1 == null || cek2 == null){           //jika salah satu variabel bernilai NULL maka ulangi proses penginputan
            println("!!!Peringatan : Masukkan Sebuah Angka!!!")
            continue
        }
        else{
            angka = input1.toDouble()               //Jika sudah benar maka simpan nilai inputan dan konversi ke masing-masing tipe data variabel
            p = input2.toInt()
            break
        }
    }while (true)


    val penentu = angka - angka.toInt()            //variabel yang nantinya akan diumpankan ke kelas objek saat objek bilangan bulat dibuat
                                                   //yang berguna untuk menentukan apakah inputan user dibulatkan atau tidak
    var x = angka                                  //penyimpanan ke variabel lain agar tidak merubah nilai variabel angka
    x = x * 10.0.pow(p)                            //proses untuk melakukan pembulatan sesuai dengan prefrensi inputan p > 0 atau p < 0
    x = floor(x + 0.5)
    x = x/10.0.pow(p)                              //Mengubah ke bentuk desimal kembali

    if(p<0) println("Hasil Pembulatan satuan/puluhan/ratusan,dst ---> $x")
    else println("Hasil Pembulatan $p angka dibelakang koma ---> $x")

    val bil_bulat = bilanganBulat(penentu)         //Pembuatan Objek Bilangan Bulat dengan mengoper variabel penentu
    bil_bulat.bilBulat = angka.toInt()

    println("Hasil Pembulatan Menjadi Bilangan Bulat --> ${bil_bulat.bilBulat}")
}
